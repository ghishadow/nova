
use kernel::{
    drm::{
        self,
        device::Device as DrmDevice
    },
    prelude::*,
};

use crate::driver::NovaDriver;

pub(crate) struct File();

impl drm::file::DriverFile for File {
    type Driver = NovaDriver;

    fn open(_dev: &DrmDevice<Self::Driver>) -> Result<Pin<Box<Self>>> {
        pr_info!("DRM Device :: open()\n");

        Ok(Box::into_pin(Box::try_new(Self())?))
    }
}
