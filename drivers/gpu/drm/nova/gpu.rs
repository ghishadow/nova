
use kernel::{
    error::code::*,
    firmware::{Firmware},
    fmt,
    io_mem::IoMem,
    pci,
    prelude::*,
    str::CString,
};

use core::{
    fmt::Debug,
};

use crate::bios::*;
use crate::driver::BAR_SIZE;

#[derive(Debug)] // TODO use strum_macros instead
pub(crate) enum Chipset {
    TU102 = 0x162,
    TU104 = 0x164,
    TU106 = 0x166,
    TU117 = 0x167,
    TU116 = 0x168,
    GA102 = 0x172,
    GA103 = 0x173,
    GA104 = 0x174,
    GA106 = 0x176,
    GA107 = 0x177,
    AD102 = 0x192,
    AD103 = 0x193,
    AD104 = 0x194,
    AD106 = 0x196,
    AD107 = 0x197,
}

#[derive(Debug)] // TODO use strum_macros instead
pub(crate) enum CardType {
    TU100 = 0x160,
    GA100 = 0x170,
    AD100 = 0x190,
}

#[allow(dead_code)]
pub(crate) struct GpuSpec {
    boot0: u64,
    card_type: CardType,
    chipset: Chipset,
    chiprev: u8
}

pub(crate) struct Gpu {
    booter_load_fw: Firmware,
    booter_unload_fw: Firmware,
    gsp_fw: Firmware,
    spec: GpuSpec,
    bar: IoMem<BAR_SIZE>,
    bios: Bios
}

// TODO replace with derive(FromPrimitive)
impl Chipset {
    fn from_u32(value: u32) -> Option<Chipset> {
        match value {
            0x162 => { Some(Chipset::TU102) },
            0x164 => { Some(Chipset::TU104) },
            0x166 => { Some(Chipset::TU106) },
            0x167 => { Some(Chipset::TU117) },
            0x168 => { Some(Chipset::TU116) },
            0x172 => { Some(Chipset::GA102) },
            0x173 => { Some(Chipset::GA103) },
            0x174 => { Some(Chipset::GA104) },
            0x176 => { Some(Chipset::GA106) },
            0x177 => { Some(Chipset::GA107) },
            0x192 => { Some(Chipset::AD102) },
            0x193 => { Some(Chipset::AD103) },
            0x194 => { Some(Chipset::AD104) },
            0x196 => { Some(Chipset::AD106) },
            0x197 => { Some(Chipset::AD107) },
            _ => None
        }
    }
}

// TODO replace with derive(FromPrimitive)
impl CardType {
    fn from_u32(value: u32) -> Option<CardType> {
        match value {
            0x160 => { Some(CardType::TU100) },
            0x170 => { Some(CardType::GA100) },
            0x190 => { Some(CardType::AD100) },
            _ => None
        }
    }
}

impl GpuSpec {
    fn new(bar: &IoMem<BAR_SIZE>) -> Result<GpuSpec>
    {
        let boot0 = u64::from_le(bar.readq(0));
        let chip = ((boot0 & 0x1ff00000) >> 20) as u32;

        if boot0 & 0x1f000000 == 0 {
            return Err(ENODEV);
        }

        let chipset = match Chipset::from_u32(chip) {
            Some(x) => { x },
            None => return Err(ENODEV)
        };

        let card_type = match CardType::from_u32(chip & 0x1f0) {
            Some(x) => { x },
            None => return Err(ENODEV)
        };

        Ok(Self {
            boot0: boot0,
            card_type: card_type,
            chipset: chipset,
            chiprev: (boot0 & 0xff) as u8,
        })
    }
}

impl Gpu {
    pub(crate) fn new(bar: IoMem<BAR_SIZE>) -> Result<Gpu>
    {
        let spec = GpuSpec::new(&bar)?;

        pr_info!("NVIDIA {:?} (0x{:#x})", spec.chipset, spec.boot0);

        Ok(Self {
            booter_load_fw: Firmware::new(),
            booter_unload_fw: Firmware::new(),
            gsp_fw: Firmware::new(),
            spec: spec,
            bar: bar,
            bios: Bios::new(),
        })
    }

    pub(crate) fn init(&mut self, dev: &mut pci::Device) -> Result
    {
        let mut chip_name = CString::try_from_fmt(fmt!("{:?}", self.spec.chipset))?;
        chip_name.make_ascii_lowercase();

        let fw_booter_load_path = CString::try_from_fmt(fmt!("nvidia/{}/gsp/booter_load-535.113.01.bin", &*chip_name))?;
        let fw_booter_unload_path = CString::try_from_fmt(fmt!("nvidia/{}/gsp/booter_unload-535.113.01.bin", &*chip_name))?;
        let fw_gsp_path = CString::try_from_fmt(fmt!("nvidia/{}/gsp/gsp-535.113.01.bin", &*chip_name))?;

        match self.booter_load_fw.request(&*fw_booter_load_path, dev) {
            Err(e) => return Err(e),
            Ok(_) => ()
        }

        match self.booter_unload_fw.request(&*fw_booter_unload_path, dev) {
            Err(e) => return Err(e),
            Ok(_) => ()
        }

        match self.gsp_fw.request(&*fw_gsp_path, dev) {
            Err(e) => return Err(e),
            Ok(_) => (),
        }

        self.bios.probe(&self.bar)?;

        self.bios.find_fwsec()?;
        Ok(())
    }
}
