
use alloc::boxed::Box;
use core::{
    format_args,
    pin::Pin,
};
use kernel::{
    bindings,
    c_str,
    device,
    driver,
    drm,
    error::code::*,
    io_mem::IoMem,
    pci, pci::define_pci_id_table,
    prelude::*,
    sync::Arc,
};

use crate::{file, gpu::Gpu};

/// BAR Size
pub(crate) const BAR_SIZE: usize = 16777216;

pub (crate) struct NovaDriver;

/// Convienence type alias for the DRM device type for this driver
pub(crate) type NovaDevice = drm::device::Device<NovaDriver>;

pub(crate) struct NovaData {
    dev: device::Device,
    //gpu: Gpu,
}

type DeviceData = device::Data<drm::drv::Registration<NovaDriver>, kernel::io_mem::Resource, NovaData>;

const INFO: drm::drv::DriverInfo = drm::drv::DriverInfo {
    major: 0,
    minor: 0,
    patchlevel: 0,
    name: c_str!("nova"),
    desc: c_str!("Nvidia Graphics"),
    date: c_str!("20240227"),
};

impl pci::Driver for NovaDriver {
    type Data = Arc<DeviceData>;

    define_pci_id_table! {
        (),
        [ (pci::DeviceId::new(bindings::PCI_VENDOR_ID_NVIDIA, bindings::PCI_ANY_ID as u32), None) ]
    }

    fn probe(pdev: &mut pci::Device, _id_info: Option<&Self::IdInfo>) -> Result<Arc<DeviceData>> {
        pr_info!("probe()\n");

        let dev = device::Device::from_dev(pdev);
        //let reg = drm::drv::Registration::<NovaDriver>::new(&dev)?;
        let reg = kernel::new_drm_registration!(NovaDriver, &dev)?;
        let reg_info = reg.registration_info();

        pdev.enable_device_mem()?;
        pdev.set_master();

        let _bars = pdev.select_bars(bindings::IORESOURCE_MEM.into());

        let res = pdev.take_resource(0).ok_or(ENXIO)?;
        let bar = unsafe { IoMem::<BAR_SIZE>::try_new(&res) }?;

        let data =
            kernel::new_device_data!(reg, res, NovaData { dev }, "Nova::Registrations")?;
        let data: Arc<DeviceData> = data.into();

        kernel::drm_device_register!(reg_info, data.clone(), 0)?;

        let mut gpu = Gpu::new(bar)?;
        let _ = gpu.init(pdev)?;

        Ok(data)
    }

    fn remove(_data: &Self::Data) {
        pr_info!("remove()\n");
    }
}

#[vtable]
impl drm::drv::Driver for NovaDriver {
    type Data = Arc<DeviceData>;
    type File = file::File;
    type Object = crate::gem::Object;

    const INFO: drm::drv::DriverInfo = INFO;
    const FEATURES: u32 = 0;

    kernel::declare_drm_ioctls! {}
}

pub(crate) struct NovaModule {
    _registration: Pin<Box<driver::Registration<pci::Adapter<NovaDriver>>>>,
}

impl kernel::Module for NovaModule {
    fn init(_name: &'static CStr, module: &'static ThisModule) -> Result<Self> {
        pr_info!("Module loaded!\n");

        let registration = driver::Registration::new_pinned(c_str!("nova"), module)?;
        pr_info!("Registerd PCI driver.\n");

        Ok(Self {
            _registration: registration,
        })
    }
}
